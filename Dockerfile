FROM gradle:4.7.0-jdk8-alpine AS build
RUN gradle build --no-daemon 

FROM node:carbon
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 8080
CMD [ "npm", "start" ]